package com.jeecg.teamrank.main.web;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.VelocityContext;
import org.jeecgframework.p3.core.util.plugin.ViewVelocity;
import org.jeecgframework.p3.core.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.jeecg.teamrank.main.entity.JpTeamPerson;
import com.jeecg.teamrank.main.service.JpTeamPersonService;
import com.jeecg.teamrank.main.util.ConfigUtil;


 /**
 * 描述：首页
 * @author scott
 * @version:1.0
 */
@Controller
@RequestMapping("/teamrank/index")
public class teamrankIndexController extends BaseController{

	 @Autowired
	  private JpTeamPersonService jpTeamPersonService;
	/**
	 * 外网-师资列表
	 * @param request
	 * @param response
	 */

	@RequestMapping(method = { RequestMethod.GET, RequestMethod.POST })
	public void getTeacherList(@ModelAttribute JpTeamPerson query, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false, value = "pageNo", defaultValue = "1") int pageNo, 
			@RequestParam(required = false, value = "pageSize", defaultValue = "10") int pageSize) throws Exception {
		try {
			LOG.info(request, "getTeacherList");
			List<JpTeamPerson> teamPersonEntities = jpTeamPersonService.findByQueryString();
			VelocityContext velocityContext = new VelocityContext();
			//图片拼接路径
	
			 String img_domain = ConfigUtil.getProperty("img_domain");
			 velocityContext.put("img_domain", img_domain);//返回域名，拼接路径
			velocityContext.put("teamPersonEntities",teamPersonEntities);
			String viewName = "teamrank/main/default/teachers.vm";
			ViewVelocity.view(request,response,viewName,velocityContext);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/**
	 * 外网.教师信息
	 * @param id
	 * @param request
	 * @param response
	 */
	
	@RequestMapping(params = "getTeacher", method = { RequestMethod.GET, RequestMethod.POST })
	public void getTeacher(@ModelAttribute JpTeamPerson query, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false, value = "pageNo", defaultValue = "1") int pageNo, 
			@RequestParam(required = false, value = "pageSize", defaultValue = "10") int pageSize) throws Exception {
		try {
			String id=request.getParameter("id");
			LOG.info(request, "getTeacher");
			JpTeamPerson teamPersonEntity =jpTeamPersonService.findByQuery(id);
			VelocityContext velocityContext = new VelocityContext();
			//图片拼接路径
			String imgSrc =null;
			if(teamPersonEntity!=null){
				 imgSrc = teamPersonEntity.getImgSrc();
			}
			 String img_domain = ConfigUtil.getProperty("img_domain")+imgSrc;
			 velocityContext.put("img_domain", img_domain);//返回域名，拼接路径
			velocityContext.put("teamPersonEntity",teamPersonEntity);
			String viewName = "teamrank/main/default/teacher.vm";
			ViewVelocity.view(request,response,viewName,velocityContext);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
/**
 * 跳转社区介绍
 */
	@RequestMapping(params = "introduce", method = { RequestMethod.GET, RequestMethod.POST })
	public void introduce(@ModelAttribute JpTeamPerson query, HttpServletRequest request, HttpServletResponse response,
			@RequestParam(required = false, value = "pageNo", defaultValue = "1") int pageNo, 
			@RequestParam(required = false, value = "pageSize", defaultValue = "10") int pageSize) throws Exception {
		try {
			VelocityContext velocityContext = new VelocityContext();
			String viewName = "teamrank/main/default/introduce.vm";
			ViewVelocity.view(request,response,viewName,velocityContext);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
